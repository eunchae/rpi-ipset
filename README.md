# RPI-IPset

라즈베리파이를 WIFI AP 로 만들어서, 그쪽으로 접속하고 API를 통해서 이더넷 IP설정을 변경할 수 있는 기능을 제공한다.

## AP 만들기

다음을 설치한다.

```
sudo apt install hostapd
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo apt install dnsmasq
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo mv /etc/dhcpcd.conf /etc/dhcpcd.conf.orig
sudo mv /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.orig
```

/etc/dhcpcd.conf 를 다음과 같이 수정한다.
```
hostname
clientid
persistent
option rapid_commit
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
option interface_mtu
require dhcp_server_identifier
slaac private

interface wlan0
    static ip_address=10.2.0.1/24
    nohook wpa_supplicant
```

/etc/hostapd/hostapd.conf 를 다음과 같이 수정한다.
```
country_code=KR
interface=wlan0
ssid=FoodJukeBox
hw_mode=g
channel=7
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=fjb0000#
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

dnsmasq 를 설정하기위해서 /etc/dnsmasq.conf 파일을 다음과 같이 수정한다.
```
interface=wlan0 
dhcp-range=10.2.0.2,10.2.0.20,255.255.255.0,24h
domain=wlan     
address=/gw.wlan/10.2.0.1
```

라즈베리파이를 재실행한다.

## API 실행하기

API는 다음과 같이 실행이 가능하다.
```
sudo node app.js
```

forever 를 사용하여 라즈베리구동시 자동으로 실행되도록 설정한다.


## IP 설정하기
1. WIFI 접속하기
    1. 푸드쥬크박스(라즈베리파이)를 구동시킨다.
    1. 폰이나 노트북에서 와이파이를 켜고, FoodJukeBox 라는 AP를 찾는다.
    1. 암호로 fjb0000# 을 사용하여 접속한다.
    1. 웹브라우저를 열고 http://10.2.0.1:10001/docs 에 접속한다.
    1. default 를 누르고 필요한 내용을 입력한다.

1. 유동아이피(공유기) 사용하기 : dhcp 사용하기
    1. body 영역에 다음과 같이 입력한다.
       ``` {"method" : "dhcp" } ```
    1. Try it out! 버튼을 누른다.
    1. 장비가 리부팅 될때까지 기다린다.

1. 고정아이피 사용하기
    1. 사용하고자 하는 고정아이피를 확인한다. 사용하고자 하는 아이피가 192.168.0.33 이고, 넷마스크가 255.255.255.0, 게이트웨이가 192.168.0.1 이라면 body 영역에 다음과 같이 입력하고,  Try it out! 버튼을 누른다.  장비가 리부팅 될때까지 기다린다.
```
{
    "method" : "static",
    "ipaddress" : "192.168.0.33",
    "netmask" : "255.255.255.0",
    "gateway" : "192.168.0.1"
}
```


## 기타
swagger_router 를 못찾겠다는 에러가 나온다면 node_modules/bagpipes/lib/fittingTypes/user.js 을 수정해야함
```
var split = err.message.split(path.sep);
```
이 부분을 아래와 같이 수정해야함
```
var split = err.message.split('\n')[0].split(path.sep);
```

